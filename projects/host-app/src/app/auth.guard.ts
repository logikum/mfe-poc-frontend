import { Injectable } from '@angular/core';
import {
  CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router
} from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
      private router: Router,
      private authService: AuthService
  ) { }

  canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): boolean|UrlTree {

    if (!this.authService.isUserLoggedIn()) {
      var urlTree = this.router.createUrlTree(['login']);
      urlTree.queryParams['retUrl'] = `/${ route.url }`;
      return urlTree;
    }
    else
      return true;
  }
}
