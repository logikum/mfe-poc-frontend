import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../auth.service';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-login',
  template: `
    <h2>Login Form</h2>
    <div>
      <form #loginForm="ngForm" (ngSubmit)="onFormSubmit(loginForm)">
        <p><span class="label">User Name:</span><input type='text'  name='username' ngModel> min. 4 characters</p>
        <p><span class="label">Password:</span><input type="password"  name="password" ngModel> min. 4 characters</p>
        <p><span class="label"></span><button type="submit">Submit</button></p>
      </form>
    </div>
  `,
  styles: [
      '.label { display: inline-block; width: 100px }',
      'button { color: white; background-color: dodgerblue; padding: .3rem 0.7rem; border: none }'
  ]
})
export class LoginComponent implements OnInit {

  private retUrl?: string;

  constructor(
    private router: Router,
    private activatedRoute:ActivatedRoute,
    private authService: AuthService,
    private http: BackendService
  ) { }

  ngOnInit() {
    this.retUrl = this.activatedRoute.snapshot.queryParams['retUrl'];
  }

  onFormSubmit(
      loginForm: { value: { username: string; password: string; }; }
  ) {
    this.http.login(
        loginForm.value.username,
        loginForm.value.password
    )
      .subscribe( token => {
        this.authService.loggedIn( loginForm.value.username, token )
        this.router.navigate( [ this.retUrl ?? 'home' ]);
      }, error => {
        alert( `Login failed: ${ error.message }` );
      });
  }
}
