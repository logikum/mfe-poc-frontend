import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(
      private http: HttpClient
  ) { }

  login(
      username: string,
      password: string
  ): Observable<string> {

    return this.http.post(
      '/api/host/login',
      { username, password },
      { responseType: 'text' }
    );
  }
}
