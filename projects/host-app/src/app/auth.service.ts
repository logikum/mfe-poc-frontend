import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isloggedIn: boolean;
  private userName?: string;
  private userToken?: string;

  get username(): string | undefined {
    return this.userName;
  }
  get token(): string | undefined {
    return this.userToken;
  }

  constructor() {
    this.isloggedIn = false;
  }

  loggedIn(
      username: string,
      token: string
  ): void {

    this.userName = username;
    this.userToken = token;
    this.isloggedIn = true;
  }

  isUserLoggedIn(): boolean {
    return this.isloggedIn;
  }

  logoutUser(): void{
    this.userToken = undefined;
    this.isloggedIn = false;
  }
}
