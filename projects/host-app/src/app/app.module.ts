import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProtectedComponent } from './protected/protected.component';
import { ConsentComponent } from './consent/consent.component';
import { BadConsentComponent } from './bad-consent/bad-consent.component';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }, {
    path: 'home',
    component: HomeComponent
  }, {
    path: 'protected',
    canActivate: [ AuthGuard ],
    component: ProtectedComponent
  }, {
    path: 'consent',
    canActivate: [ AuthGuard ],
    component: ConsentComponent
  }, {
    path: 'bad-consent',
    component: BadConsentComponent
  }, {
    path: 'about',
    component: AboutComponent
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: '**',
    component: HomeComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProtectedComponent,
    ConsentComponent,
    BadConsentComponent,
    AboutComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot( routes, { enableTracing: false } ),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [
      AppComponent
  ]
})
export class AppModule { }
