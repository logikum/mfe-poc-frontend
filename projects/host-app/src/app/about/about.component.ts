import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  template: `
    <h2>About</h2>
    <p>ठिठुरन भरी सर्दी का यह सिलसिला इस पूरे हफ्ते जारी रहने का अनुमान है उसके बाद कुछ राहत महसूस होगी। राजधानी दिल्ली में अगले दो दिनों तक शीतलहर का प्रकोप रहेगा। न्यूनतम तापमान महज तीन से चार डिग्री के आसपास रह सकता है।</p>
  `,
  styles: [
  ]
})
export class AboutComponent { }
