import { Component } from '@angular/core';

@Component({
  selector: 'app-protected',
  template: `
    <h2>Protected</h2>
    <p>ト－クゲストは、芸歴46年にして今も第一線で活躍し続ける“ピン芸人のレジェンド”関根勤！<br>
      「山口百恵とコントを演じた」など、お笑いの歴史を生き抜いてきた関根が自らのキャリアを振り返りながら明かす、芸能界で長くやり続ける秘訣とは？<br>
      FUJIWARAの藤本が芸人ディレクターとなり、キックボクシング世界王者の那須川天心に密着取材！<br>
      ボクシングへの転向を考える天心は何を思い、どこへ向かうのか？</p>
  `,
  styles: [
  ]
})
export class ProtectedComponent { }
