import { Component } from '@angular/core';

@Component({
  selector: 'app-bad-consent',
  template: `
    <h2>Consent without Authentication</h2>
    <p>Bifreiðin, sem varð fyrir skemmdum, er í eigu borgarstjóra og fjölskyldu hans en málið kom upp um helgina. Samkvæmt heimildum fréttastofu er talið að skotið hafi verið fleiri en einu skoti á bílinn og að notaður hafi verið riffill. </p>
    <iframe src="http://localhost:4201/start/00000000-0000-0000-0000-000000000000"></iframe>
  `,
  styles: [
    'iframe { width: 100%; min-height: 400px; border: none }'
  ]
})
export class BadConsentComponent { }
