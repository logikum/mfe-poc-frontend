import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-consent',
  template: `
    <h2>Consent</h2>
    <p>If you grant your consent for the examination, follow the instructions below:</p>
    <iframe [src]="consentUrl"></iframe>
  `,
  styles: [
      'iframe { width: 100%; min-height: 400px; border: none }'
  ]
})
export class ConsentComponent {

  get consentUrl(): SafeResourceUrl {
    const dangerousUrl = `http://localhost:4201/start/${ this.authService.token }`;
    const safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(dangerousUrl);
    return safeUrl;
  }

  constructor(
    private sanitizer: DomSanitizer,
    private authService: AuthService
  ) { }
}
