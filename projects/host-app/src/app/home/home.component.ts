import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-home',
  template: `
    <h2>Home</h2>
    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nunc in suscipit lectus, et euismod mi. Nunc auctor sollicitudin hendrerit. Ut molestie nibh a volutpat consectetur. Nam consectetur erat a odio ultricies, sed pretium nibh congue. Morbi dui nibh, consectetur ac magna in, sodales cursus purus. Nulla eu nibh massa. Nullam sed egestas elit. Morbi in justo in libero lobortis pretium.</p>
    <p *ngIf="isLoggedIn">
      <a (click)="logout()" href="#">Logout {{ username }}</a>
    </p>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  get username(): string | undefined {
    return  this.authService.username;
  }

  constructor(
      private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  get isLoggedIn() {
    return this.authService.isUserLoggedIn();
  }

  logout() {
    this.authService.logoutUser();
  }
}
