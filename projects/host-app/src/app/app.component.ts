import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <!--The content below is only a placeholder and can be replaced.-->
    <div style="text-align:center" class="content">
      <h1>Patient Portal</h1>
      <div class="menu">
        <a [routerLink]="'home'">Home</a>
        <a [routerLink]="'protected'">Protected</a>
        <a [routerLink]="'consent'">Consent</a>
        <a [routerLink]="'bad-consent'">Consent w/o auth</a>
        <a [routerLink]="'about'">About</a>
      </div>
    </div>
    <div class="page">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [
    '.menu { background-color: dodgerblue; height: 2rem }',
    'a { margin: .75rem; color: white; font-weight: bold; line-height: 1.8rem; text-decoration: none }',
    '.page { width: 800px; margin: auto }'
  ]
})
export class AppComponent { }
