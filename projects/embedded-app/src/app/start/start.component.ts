import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BackendService } from '../backend.service';

@Component({
  selector: 'app-start',
  template: `
    <p *ngIf="error; else wait" class="error">{{ error.message }}</p>
    <ng-template #wait>
      <p>Please wait for a moment...</p>
    </ng-template>
  `,
  styles: [
    '.error { color: red; border: red solid 2px; padding: .3rem .7rem }'
  ]
})
export class StartComponent implements OnInit {

  token: string | null = null;
  error?: Error;

  constructor(
      private readonly router: Router,
      private readonly route: ActivatedRoute,
      private readonly http: BackendService
  ) {
    this.token = this.route.snapshot.paramMap.get( 'token' );
  }

  ngOnInit(): void {
    this.http.getPatient( this.token ?? '' )
      .subscribe( patient => {
        if (patient) {
          this.http.patient = patient;
          this.router.navigate(['consent-form']);
        } else {
          this.error = new Error( 'Please log in the Patient Portal first!' );
        }
      }, error => {
        this.error = error;
      });
  }
}
