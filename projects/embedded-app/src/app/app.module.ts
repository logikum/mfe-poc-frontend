import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { StartComponent } from './start/start.component';
import { ConsentFormComponent } from './consent-form/consent-form.component';
import { SuccessComponent } from './success/success.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';

const routes: Routes = [
  {
  //   path: '',
  //   redirectTo: 'home',
  //   pathMatch: 'full'
  // }, {
    path: 'start/:token',
    component: StartComponent
  }, {
    path: 'consent-form',
    component: ConsentFormComponent
  }, {
    path: 'success',
    component: SuccessComponent
  }, {
    path: '**',
    component: UnauthorizedComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    ConsentFormComponent,
    SuccessComponent,
    UnauthorizedComponent,
    StartComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot( routes, { enableTracing: false } ),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [
      AppComponent
  ]
})
export class AppModule { }
