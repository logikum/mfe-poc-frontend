import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="header">
      <span class="title">Consent app is running!</span>
    </div>
    <router-outlet></router-outlet>
  `,
  styles: [
    '.header { text-align: center; background-color: dodgerblue }',
    '.title { display: block; color: white; padding: .3rem .7rem }'
  ]
})
export class AppComponent { }
