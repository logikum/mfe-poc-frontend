import { Component } from '@angular/core';

@Component({
  selector: 'app-unauthorized',
  template: `
    <p class="error">Please log in the Patient Portal first!</p>
  `,
  styles: [
    '.error { color: red; border: red solid 2px; padding: .3rem .7rem }'
  ]
})
export class UnauthorizedComponent { }
