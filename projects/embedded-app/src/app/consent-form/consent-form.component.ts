import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { BackendService } from '../backend.service';

@Component({
  selector: 'app-home',
  template: `
    <h2>Consent Form</h2>
    <p>Please fill out the form below::</p>
    <form #consentForm="ngForm" (ngSubmit)="onFormSubmit(consentForm)">
      <p><span class="label">Most valuable player:</span><input type='text' name='mvpName' #mvpName></p>
      <p><span class="label">Best offensive player:</span><input type='text' name='offName'></p>
      <p><span class="label">Best defensive player:</span><input type='text' name='defName'></p>
      <p><span class="label"></span><button type="submit">Submit</button></p>
    </form>
  `,
  styles: [
    '.label { display: inline-block; width: 150px }',
    'button { color: white; background-color: dodgerblue; padding: .3rem 0.7rem; border: none }'
  ]
})
export class ConsentFormComponent implements AfterViewInit  {

  @ViewChild('mvpName') mvpName: ElementRef = new ElementRef<any>('mvpName');

  constructor(
    private readonly router: Router,
    private readonly http: BackendService
  ) { }

  ngAfterViewInit(): void {
    this.mvpName.nativeElement.value = this.http.patient?.patientName;
  }

  onFormSubmit(
    consentForm: { value: { mvpName: string; offName: string; defName: string; }; }
  ) {
    this.router.navigate( ['success']);
  }
}
