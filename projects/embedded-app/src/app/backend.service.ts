import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { PatientDto } from './patient.dto';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  patient?: PatientDto;

  constructor(
      private http: HttpClient
  ) { }

  getPatient(
      token: string
  ): Observable<PatientDto> {

    return this.http.get<PatientDto>(
        `/api/patient/${ token }`
    );
  }
}
