import { Component } from '@angular/core';

@Component({
  selector: 'app-success',
  template: `
    <h2>Success</h2>
    <p>Your consent has been saved successfully.</p>
  `,
  styles: [
  ]
})
export class SuccessComponent { }
